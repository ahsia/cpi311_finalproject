using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Common;

namespace CPI311
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class CPI311 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;        // Drawing 2D stuff
        Texture2D bombTexture;          // Object for a texture
        Texture2D fishTexture;
        SpriteFont spriteFont;            // Object for a font
        BasicEffect effect;
        int gameSpeed = 2;
        Common.Plane plane;
        String[] directions = new String[8];
        String direction;
        Camera camera;
        Camera TCamera;                 //Third person Camera
        Camera FCamera;                 //First person Camera

        //Frame rate counter
        int frameRate = 0;
        int frameCounter = 0;
        TimeSpan Time = TimeSpan.Zero;

        //Sun Object
        ModelObject torus;
        //Camera model object
        ModelObject Cube;

        //Planet Object List
        Planet mercury;
        Planet earth;
        Planet moon;
       
        public CPI311()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // TODO: use this.Content to load your game content here

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            bombTexture = Content.Load<Texture2D>("Textures/Bomb");      // Load the texture
            spriteFont = Content.Load<SpriteFont>("Fonts/SegoeUI");    // Load the font
            fishTexture = Content.Load<Texture2D>("Textures/Jellyfish");
            direction = directions[0];
            
            //Direction Array filled
            directions[0] = "N"; 
            directions[1] = "NE";
            directions[2] ="E";
            directions[3] ="SE";
            directions[4] ="S";
            directions[5] ="SW";
            directions[6] ="W";
            directions[7] ="NW";
            
            //Sun
            torus = new ModelObject();
            torus.Model = Content.Load<Model>("Models/Sphere");
            torus.Texture = fishTexture;
            torus.Scale *= 5;
            
            //Camera cube
            Cube = new ModelObject();
            Cube.Model = Content.Load<Model>("Models/Cube");
            Cube.Texture = Content.Load<Texture2D>("Textures/Stripes");
            Cube.Scale *= 2;
            Cube.Position = new Vector3(0, -10, -20);

            plane = new Common.Plane(99);
            plane.Texture = Content.Load<Texture2D>("Textures/Jellyfish");
            plane.Scale *= 50;
            plane.Position = new Vector3(0, -15, 0);

            camera = new Camera();
            camera.Position = Cube.Position;
            camera.AspectRatio = GraphicsDevice.Viewport.AspectRatio;

            FCamera = new Camera();
            FCamera.Position = Cube.Position;
            FCamera.AspectRatio = GraphicsDevice.Viewport.AspectRatio;

            TCamera = new Camera();
            TCamera.Position = new Vector3(0, 40, 0);
            TCamera.RotateX = 1.5f;
            TCamera.AspectRatio = GraphicsDevice.Viewport.AspectRatio;
            
            /// <summary>
            /// A list of all Planet classes.
            /// Each planet revolves around the mother object creating a Solar System model
            /// </summary>
            //Mercury
            mercury = new Planet();
            mercury.Parent = torus;
            mercury.RevolutionRate = MathHelper.PiOver2*gameSpeed;
            mercury.Radius = 15;
            mercury.Scale = (torus.Scale/5)*2;
            mercury.Model = torus.Model;
            mercury.Texture = bombTexture;
            mercury.gamespeed = gameSpeed;
            //Earth
            earth = new Planet();
            earth.Parent = torus;
            earth.RevolutionRate = MathHelper.PiOver4*gameSpeed;
            earth.Radius = 17;
            earth.Scale *= 6 ;
            earth.Model = torus.Model;
            earth.Texture = torus.Texture;
            earth.RotationRate *= 2;
            earth.gamespeed = gameSpeed;
            //Moon
            moon = new Planet();
            moon.Parent = earth;
            moon.RevolutionRate = MathHelper.PiOver4*gameSpeed;
            moon.Radius = 19;
            moon.Scale *= 1;
            moon.Model = torus.Model;
            moon.Texture = bombTexture;
            moon.gamespeed = gameSpeed;
                                               
            effect = new BasicEffect(GraphicsDevice);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
           //Updates Frame rate counter
            Time += gameTime.ElapsedGameTime;

            if (Time > TimeSpan.FromSeconds(1))
            {
                Time -= TimeSpan.FromSeconds(1);
                frameRate = frameCounter;
                frameCounter = 0;
            }
           // TODO: Add your update logic here
            //All keybard inputs                      
            KeyboardState keyboardState = Keyboard.GetState();
            if (keyboardState.IsKeyDown(Keys.Escape))
                this.Exit();
            if (keyboardState.IsKeyDown(Keys.W))
            {
                Cube.Position += 0.11f * Vector3.UnitZ;
                FCamera.Position += 0.11f * Vector3.UnitZ;
            }
            if (keyboardState.IsKeyDown(Keys.S))
            {
                Cube.Position -= 0.11f * Vector3.UnitZ;
                FCamera.Position -= 0.11f * Vector3.UnitZ;
            }
            if (keyboardState.IsKeyDown(Keys.A))
            {
                Cube.Position -= 0.01f * Vector3.UnitX;
                FCamera.Position -= 0.01f * Vector3.UnitX;
            }
            if (keyboardState.IsKeyDown(Keys.D))
            {
                Cube.Position += 0.01f * Vector3.UnitX;
                FCamera.Position += 0.01f * Vector3.UnitX;
            }
            if (keyboardState.IsKeyDown(Keys.Up))
            {
                FCamera.RotateX = 0.01f;
            }
            if (keyboardState.IsKeyDown(Keys.Down))
                FCamera.RotateX = -0.01f;
            //Have to switch between the cameras once for these to work******
            if (keyboardState.IsKeyDown(Keys.Right))
            {
                Cube.RotateY = -0.01f;
                FCamera.RotateY = -0.01f;
            }
            if (keyboardState.IsKeyDown(Keys.Left))
            {
                Cube.RotateY = 0.01f;
                FCamera.RotateY = 0.01f;
            }
            //*************************************************************
            if (keyboardState.IsKeyDown(Keys.Add))
                camera.FieldOfView -= 0.01f;
            if (keyboardState.IsKeyDown(Keys.Subtract))
                camera.FieldOfView += 0.01f;
            
            //Switches inbetween the first/third camera when Tab is pressed.
            if (keyboardState.IsKeyDown(Keys.Tab))
            {
                    if (camera == FCamera)
                    {
                        camera = TCamera;
                    }
                    else
                        camera = FCamera;
            }
            
            //Is suppose to speedup/slow down the animation of the planets
            // When < and > are pressed.
            if (keyboardState.IsKeyDown(Keys.M))
                gameSpeed = 4;
            /*else
                gameSpeed = 2;*/
            if (keyboardState.IsKeyDown(Keys.OemComma))
                gameSpeed = 1;
            else
                gameSpeed = 2;

            //updates all the rotation of all planets
            mercury.Update(gameTime);
            earth.Update(gameTime);
            moon.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // Clear the screen
            GraphicsDevice.Clear(Color.Blue);
            // Set the Depth Stencil State to default for 3D rendering
            GraphicsDevice.DepthStencilState = new DepthStencilState();
            // Setup some colors for lighting
            effect.EmissiveColor = new Vector3(0.2f, 0.2f, 0.2f);
            effect.DiffuseColor = new Vector3(0.5f, 0.0f, 0.0f);
            effect.SpecularColor = new Vector3(0.0f, 0.5f, 0.0f);
            effect.SpecularPower = 10;
            effect.LightingEnabled = true; // Enable lighting!
            // Some parameters for the Directional lighting
            effect.DirectionalLight0.Direction = new Vector3(0, -1, 1);
            effect.DirectionalLight0.SpecularColor = Vector3.One;

            // Provide the different matrices

                effect.View = camera.View;
                effect.Projection = camera.Projection;
          
            effect.World = torus.World;
            effect.Texture = torus.Texture;
            effect.TextureEnabled = true;
            //torus.Draw(effect);
            torus.Draw(camera.View, camera.Projection);
            
            //Draws the flat plane
            effect.World = plane.World;
            effect.Texture = plane.Texture;
            effect.TextureEnabled = true;
            plane.Draw(effect);

            //The cube
            effect.World = Cube.World;
            effect.Texture = Cube.Texture;
            effect.TextureEnabled = true;
            Cube.Draw(camera.View, camera.Projection);
            // Merceruy 
            effect.World = mercury.World;
            effect.Texture = mercury.Texture;
            mercury.Draw(effect);


            // Earth 
             effect.World = earth.World;
             effect.Texture = earth.Texture;
             earth.Draw(effect);
             // Moon 
             effect.World = moon.World;
             effect.Texture = moon.Texture;
             moon.Draw(effect);
            spriteBatch.Begin();
            // Draw some string
            frameCounter++;
            string fps = string.Format("fps: {0}", frameRate);
            spriteBatch.DrawString(spriteFont, fps, new Vector2(32, 32), Color.White);
            //string blah = string.Format("Current Direction: {0}", direction);
            string speed = string.Format("game speed: {0}", gameSpeed);
            spriteBatch.DrawString(spriteFont, speed, new Vector2(50, 50), Color.White);
           // spriteBatch.DrawString(spriteFont, blah, new Vector2(50, 70), Color.White);
            spriteBatch.End();

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
