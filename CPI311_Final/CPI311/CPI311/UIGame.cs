﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.VisualBasic.PowerPacks;
using Common;

namespace CPI311
{
    public enum GameState { Start, Play, Pause, Win, Lose, Information, Credits, Menu, spellInventory, Story}

    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class UIGame : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;        // Drawing 2D stuff
        #region Textures
        //Textures
        Texture2D bombTexture;          // Object for a texture
        Texture2D Redtext;
        Texture2D Whitetext;
        Texture2D Blacktext;
        Texture2D[] iconList = new Texture2D[7];
        Texture2D spellSelect;
        String[] spellbuttons = {"1", "2", "3","4", "In use"};
        #endregion
        #region Health and Mana
            //Frames
            Rectangle heartPos;
            Rectangle healthRectangle;
            Rectangle manaRectangle;
            Rectangle FhealthRectangle;
            Rectangle FmanaRectangle;

            Texture2D healthTexture;
            Texture2D heartTex;
            Texture2D healthBar;
            Texture2D healthFrame;
            Texture2D ManaFrame;
            int maxMana = 100;
            int maxHealth = 100;
            int currHealth;
            int currMana;
            float realMana = 100;
            float realHealth = 100;
            int lives = 3;
            bool inCombat = false;
            //UI bars
            InfoBar Healthbar;
            InfoBar Lives;
            //InfoBar Mana;
        #endregion
        SpriteFont textFont;            // Object for a font
        BasicEffect effect;
        bool[] hasSpell;
        Spells spell;
        Spell spell1;
        Spell spell2;
        Spell spell3;
        Spell spell4;
        Spell currentSpell;
        Model sphere;     //Allen 

        #region Health and mana variables

        #endregion
        float imageAngle = 0;           // Rotation Angle
        float scaleAngle = 0;           // Scaling "angle"


        Common.Plane plane;
        Common.HeightMap heightMap;
        Common.BitmapWorld bitmapWorld;
        Texture2D world;

        KeyboardState keyboardState = Keyboard.GetState();
        KeyboardState prevKeyboardState = Keyboard.GetState();
        MouseState prevMouseState = Mouse.GetState();
        #region Music
        Song background1;
        #endregion
        #region Enemies
        Enemy allen, allen1, allen2, allen3, allen4;
        List<Enemy> enemyList = new List<Enemy>();
        #endregion
        #region Collision
        
        RigidObject WallTest = new RigidObject();
        RigidObject Wall0 = new RigidObject();
        RigidObject Wall1 = new RigidObject();
        RigidObject Wall2 = new RigidObject();
        RigidObject Wall3 = new RigidObject();
        RigidObject Wall4 = new RigidObject();
        RigidObject Wall5 = new RigidObject();
        RigidObject Wall6 = new RigidObject();
        RigidObject Wall7 = new RigidObject();

        List<RigidObject> Walls = new List<RigidObject>();
        List<RigidObject> deadAmmo = new List<RigidObject>();
        //new Vector3(0, plane.Position.Y + 1, -10);
        Vector3[] vertices = { new Vector3(-1, -1, -1), new Vector3(1, -1, -1), new Vector3(1, -1, 1), new Vector3(-1, -1, 1),
                               new Vector3(-1, 1, -1), new Vector3(1, 1, -1), new Vector3(1, 1, 1), new Vector3(-1, 1, 1)};
        int[] indices = { 0, 1, 2,   0, 2, 3, 
                            1, 5, 6,   1, 6, 2, 
                            5, 4, 7,   5, 7, 6, 
                            4, 0, 3,   4, 3, 7, 
                            4, 5, 1,   4, 1, 0, 
                            3, 2, 6,   3, 6, 7 };
        #endregion
        #region Firing
        List<float> deleteSphere = new List<float>();
        List<RigidObject> ammunition = new List<RigidObject>();
        #endregion
        //Camera
        Camera camera;
        Player Cube;

        Dictionary<GameState, UIElement> elements;
        GameState gameState = GameState.Start;

        public UIGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            elements = new Dictionary<GameState, UIElement>();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spell = new Spells(Content);
            background1 = Content.Load<Song>("Sounds/Theme1");
            #region Loads all the textures that will be used
                bombTexture = Content.Load<Texture2D>("Textures/Bomb");      //Menu Selection Icon
                heartTex = Content.Load<Texture2D>("Textures/Heart");        //Lives
                healthBar = Content.Load<Texture2D>("Textures/HealthBar");   //Mana/Healthbar
                healthFrame = Content.Load<Texture2D>("Textures/HealthFrame");
                ManaFrame = Content.Load<Texture2D>("Textures/ManaFrame");
                Redtext = Content.Load<Texture2D>("Textures/Red");           // Red Texture
                Whitetext = Content.Load<Texture2D>("Textures/White");       //Black Texture
                Blacktext = Content.Load<Texture2D>("Textures/Black");
                textFont = Content.Load<SpriteFont>("Fonts/SegoeUI");    // Load the font
                healthTexture = Content.Load<Texture2D>("Textures/Red");
                spellSelect = Content.Load<Texture2D>("Textures/Spell selection");
                
                // List of icons used for spells.
                iconList[0] = Content.Load<Texture2D>("Textures/Icons/Fireball");
                iconList[1] = Content.Load<Texture2D>("Textures/Icons/Air bolt");
                iconList[2]= Content.Load<Texture2D>("Textures/Icons/earth");
                iconList[3] = Content.Load<Texture2D>("Textures/Icons/Waterball");
                iconList[4]=  Content.Load<Texture2D>("Textures/Icons/Energy");
                iconList[5] = Content.Load<Texture2D>("Textures/Icons/Lava");
                iconList[6] = Content.Load<Texture2D>("Textures/Icons/Obsidian1");
                               
                hasSpell = new bool[iconList.Length];
                //Allows the original 4 elements to be drawn in inventory
                //Others will not be drawn.
                for (int i = 0; i < iconList.Length; i++)
                {
                    if (i < 4)
                        hasSpell[i] = true;
                    else
                        hasSpell[i] = false;
                }


                //plane
                plane = new Common.Plane(99);
                plane.Texture = Content.Load<Texture2D>("Textures/grassHeight");
                plane.Scale *= 50;
                plane.Position = new Vector3(0, -5, 0);
                world = Content.Load<Texture2D>("Textures/grassHeight");

                heightMap = new HeightMap(world);
                heightMap.Scale *= 50;
                bitmapWorld = new BitmapWorld(world);

            #endregion
                
            #region UI Bar Frames
                heartPos = new Rectangle(20, 20, 30, 30);
                Lives = new InfoBar(heartTex, heartPos, Color.White);
                Healthbar = new InfoBar(healthBar, healthRectangle, Color.White);
                currHealth = maxHealth;
                currMana = (int)realMana;
            #endregion

            #region Camera and Models

            sphere = Content.Load<Model>("Models/Sphere");

            //Camera cube to be replaced with the model
            Cube = new Player();
            Cube.Model = Content.Load<Model>("Models/Dude/Dude");
            Cube.Texture = Content.Load<Texture2D>("Textures/Stripes");
            Cube.StartAnimation("Take 001");
            Cube.Scale *= .1f;
            Cube.Position = new Vector3(-40, plane.Position.Y + 1, 40);
            #region enemyList
            allen = new Enemy(spell.fire);
            allen.Model = Content.Load<Model>("Models/Dude/Dude");
            allen.Texture = Content.Load<Texture2D>("Textures/Stripes");
            allen.StartAnimation("Take 001");
            allen.Scale *= .1f;
            allen.Position = new Vector3(30, plane.Position.Y + 1, 10);
            allen.player = Cube;
            enemyList.Add(allen);

            allen1 = new Enemy(spell.energy);
            allen1.Model = Content.Load<Model>("Models/Dude/Dude");
            allen1.Texture = Content.Load<Texture2D>("Textures/Stripes");
            allen1.StartAnimation("Take 001");
            allen1.Scale *= .1f;
            allen1.Position = new Vector3(10, plane.Position.Y + 1, 20);
            allen1.player = Cube;
            enemyList.Add(allen1);

            allen2 = new Enemy(spell.energy);
            allen2.Model = Content.Load<Model>("Models/Dude/Dude");
            allen2.Texture = Content.Load<Texture2D>("Textures/Stripes");
            allen2.StartAnimation("Take 001");
            allen2.Scale *= .1f;
            allen2.Position = new Vector3(10, plane.Position.Y + 1, 0);
            allen2.player = Cube;
            enemyList.Add(allen2);

            allen3 = new Enemy(spell.energy);
            allen3.Model = Content.Load<Model>("Models/Dude/Dude");
            allen3.Texture = Content.Load<Texture2D>("Textures/Stripes");
            allen3.StartAnimation("Take 001");
            allen3.Scale *= .1f;
            allen3.Position = new Vector3(15, plane.Position.Y + 1, 15);
            allen3.player = Cube;
            enemyList.Add(allen3);

            allen4 = new Enemy(spell.energy);
            allen4.Model = Content.Load<Model>("Models/Dude/Dude");
            allen4.Texture = Content.Load<Texture2D>("Textures/Stripes");
            allen4.StartAnimation("Take 001");
            allen4.Scale *= .1f;
            allen4.Position = new Vector3(30, plane.Position.Y + 1, -15);
            allen4.player = Cube;
            enemyList.Add(allen4);
            #endregion

            // TODO: use this.Content to load your game content here


            camera = new Camera();
            //3rd person
            //camera.Position = new Vector3(Cube.Position.X, Cube.Position.Y+8, Cube.Position.Z + 8);
            camera.Position = new Vector3(Cube.Position.X, Cube.Position.Y + 30, Cube.Position.Z + 1);
            camera.LookAt = Vector3.Normalize(Vector3.Subtract(Cube.Position, camera.Position));
            camera.AspectRatio = GraphicsDevice.Viewport.AspectRatio;
            camera.FarPlane = 200;


          
            #endregion
            #region  walls


            //center large wall
            WallTest.Model = Content.Load<Model>("Models/Cube");
            WallTest.Texture = Content.Load<Texture2D>("Textures/castletexture");
            WallTest.Position = new Vector3(-4f, plane.Position.Y + 2, 7);
            WallTest.Scale *= new Vector3(10.75f, 6, 43f);

            //left most side wall
            Wall0.Model = Content.Load<Model>("Models/Cube");
            Wall0.Texture = Content.Load<Texture2D>("Textures/castletexture");
            Wall0.Position = new Vector3(-50f, plane.Position.Y + 2, 0);
            Wall0.Scale *= new Vector3(1, 6, 49f);


            //right most side wall
            Wall1.Model = Content.Load<Model>("Models/Cube");
            Wall1.Texture = Content.Load<Texture2D>("Textures/castletexture");
            Wall1.Position = new Vector3(50, plane.Position.Y + 2, 0);
            Wall1.Scale *= new Vector3(1, 6, 49f);

            //right horizontal wall
            Wall2.Model = Content.Load<Model>("Models/Cube");
            Wall2.Texture = Content.Load<Texture2D>("Textures/castletexture");
            Wall2.Position = new Vector3(32.5f, plane.Position.Y + 2, -5);
            Wall2.Scale *= new Vector3(12, 6, 4);

            //south most side wall
            Wall3.Model = Content.Load<Model>("Models/Cube");
            Wall3.Texture = Content.Load<Texture2D>("Textures/castletexture");
            Wall3.Position = new Vector3(0, plane.Position.Y + 2, 50);
            Wall3.Scale *= new Vector3(50, 6, 1f);

            //north most side wall
            Wall4.Model = Content.Load<Model>("Models/Cube");
            Wall4.Texture = Content.Load<Texture2D>("Textures/castletexture");
            Wall4.Position = new Vector3(0, plane.Position.Y + 2, -50);
            Wall4.Scale *= new Vector3(50, 6, 1f);


            //left southern horizonatl wall
            Wall5.Model = Content.Load<Model>("Models/Cube");
            Wall5.Texture = Content.Load<Texture2D>("Textures/castletexture");
            Wall5.Position = new Vector3(-38, plane.Position.Y + 2, 22);
            Wall5.Scale *= new Vector3(10.25f, 6, 1.5f);

            //left northern horizontal wall
            Wall6.Model = Content.Load<Model>("Models/Cube");
            Wall6.Texture = Content.Load<Texture2D>("Textures/castletexture");
            Wall6.Position = new Vector3(-29.75f, plane.Position.Y + 2, -33);
            Wall6.Scale *= new Vector3(15, 6, 3f);

            // middle thin wall
            Wall7.Model = Content.Load<Model>("Models/Cube");
            Wall7.Texture = Content.Load<Texture2D>("Textures/castletexture");
            Wall7.Position = new Vector3(19, plane.Position.Y + 2, -5.5f);
            Wall7.Scale *= new Vector3(2f, 6, 43.5f);

            Walls.Add(WallTest);
            Walls.Add(Wall0);
            Walls.Add(Wall1);
            Walls.Add(Wall2);
            Walls.Add(Wall3);
            Walls.Add(Wall4);
            Walls.Add(Wall5);
            Walls.Add(Wall6);
            Walls.Add(Wall7);
            #endregion
            #region Menu Stuff
            #region UI elements
                    UIElement background = new UIElement(Content.Load<Texture2D>("Screens/background"),
                                                         GraphicsDevice.Viewport.Bounds);
                    UIElement win = new UIElement(Content.Load<Texture2D>("Screens/winning"),
                                           GraphicsDevice.Viewport.Bounds);
                    UIElement lose = new UIElement(Content.Load<Texture2D>("Screens/losing"),
                               GraphicsDevice.Viewport.Bounds);
                    UIElement information = new UIElement(Content.Load<Texture2D>("Screens/kemistry controls"),
                                           GraphicsDevice.Viewport.Bounds);
                    UIElement credits = new UIElement(Content.Load<Texture2D>("Screens/credits"),
                                            GraphicsDevice.Viewport.Bounds);
                    UIElement game = new UIElement(null, null);
                    UIElement spellList = new UIElement(Content.Load<Texture2D>("Textures/Black"),
                                             GraphicsDevice.Viewport.Bounds);
                    UIElement story = new UIElement(Content.Load<Texture2D>("Screens/story"),
                                           GraphicsDevice.Viewport.Bounds);
                #endregion
                #region Button Groups
                    #region Background
                        //button group for background
                        ButtonGroup backgroundgroup = new ButtonGroup();
                        backgroundgroup.Children.Add(new Button(null,
                                            new Rectangle(40, 40, 200, 25),
                                            Content.Load<Texture2D>("Textures/Bomb"),
                                            "Play", textFont));

                        backgroundgroup.Children.Add(new Button(null,
                                            new Rectangle(40, 80, 200, 25),
                                            Content.Load<Texture2D>("Textures/Bomb"),
                                            "Story", textFont));

                        backgroundgroup.Children.Add(new Button(null,
                                            new Rectangle(40, 120, 200, 25),
                                            Content.Load<Texture2D>("Textures/Bomb"),
                                            "Information", textFont));

                        backgroundgroup.Children.Add(new Button(null,
                                            new Rectangle(40, 160, 200, 25),
                                            Content.Load<Texture2D>("Textures/Bomb"),
                                            "Credits", textFont));

                        backgroundgroup.Children.Add(new Button(null,
                                            new Rectangle(40, 200, 200, 25),
                                            Content.Load<Texture2D>("Textures/Bomb"),
                                            "Quit", textFont));


                        backgroundgroup.Children[0].Clicked += Story;
                        backgroundgroup.Children[1].Clicked += Story;
                        backgroundgroup.Children[2].Clicked += Information;
                        backgroundgroup.Children[3].Clicked += Credits;
                        backgroundgroup.Children[4].Clicked += EndGame;
                        background.Children.Add(backgroundgroup);
                    #endregion
                    #region Rtmenu group
                        //button group for information and credit to return to menu
                        ButtonGroup Rtmenugroup = new ButtonGroup();

                        Rtmenugroup.Children.Add(new Button(null,
                                            new Rectangle(250, 400, 200, 25),
                                            Content.Load<Texture2D>("Textures/Bomb"),
                                            "Return to Main Menu", textFont));
        
                            Rtmenugroup.Children[0].Clicked += MainMenu;
                        
                        information.Children.Add(Rtmenugroup);
                        credits.Children.Add(Rtmenugroup);
                        
                    #endregion
                    #region Spells
                        ButtonGroup spells = new ButtonGroup();
                        spells.Children.Add(new Button(null, new Rectangle(50,50,20,20), iconList[0], "1", textFont));
                        spells.Children.Add(new Button(null, new Rectangle(200, 50, 20, 20), iconList[1], "2", textFont));
                        spells.Children.Add(new Button(null, new Rectangle(50, 250, 20, 20), iconList[2], "3", textFont));
                        spells.Children.Add(new Button(null, new Rectangle(200, 250, 20, 20), iconList[3], "4", textFont));
                        spells.Children.Add(new Button(null, new Rectangle(50, 450, 20, 20), iconList[4], "5", textFont));
                        spells.Children.Add(new Button(null, new Rectangle(200, 450, 20, 20), iconList[5], "6", textFont));
                        spells.Children.Add(new Button(null, new Rectangle(950, 450, 20, 20), iconList[6], "7", textFont));
                        spellList.Children.Add(spells);
                        
                    #endregion
                #region story
                        ButtonGroup startGame = new ButtonGroup();

                        startGame.Children.Add(new Button(null,
                                            new Rectangle(250, 360, 200, 25),
                                            Content.Load<Texture2D>("Textures/Bomb"),
                                            "Return to Main Menu", textFont));

                        startGame.Children.Add(new Button(null,
                                            new Rectangle(250, 400, 200, 25),
                                            Content.Load<Texture2D>("Textures/Bomb"),
                                            "Start Game", textFont));
                            startGame.Children[0].Clicked += MainMenu;
                            startGame.Children[1].Clicked += StartNewGame;
                        
                        story.Children.Add(startGame);


                #endregion

                #endregion


                elements[GameState.Start] = background;
                elements[GameState.Story] = story;
                elements[GameState.Play] = game;
                elements[GameState.Pause] = background;
                elements[GameState.Credits] = credits;
                elements[GameState.Information] = information;
                elements[GameState.Menu] = background;
                elements[GameState.spellInventory] = spellList;
                elements[GameState.Lose] = lose;
                elements[GameState.Win] = win;

                effect = new BasicEffect(GraphicsDevice);
            #endregion
            spell1 = spell.fire;
            spell2 = spell.air;
            spell3 = spell.earth;
            spell4 = spell.water;
            currentSpell = spell.none;
            
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            #region Current health and mana
                healthRectangle = new Rectangle(450+125, 30, currHealth*2, 20);
                manaRectangle = new Rectangle(450+125, 60, currMana, 15);
                currHealth = (int)MathHelper.Clamp(currHealth, 0, maxHealth);
                currMana = (int)MathHelper.Clamp(currHealth, 0, maxMana);
                FhealthRectangle = new Rectangle(450+125, 30, maxHealth * 2, 20);
                FmanaRectangle = new Rectangle(450+125, 60, maxMana, 15);

                if (inCombat == false)
                {
                    if (realMana < maxMana)
                    {
                        realMana += 3 * gameTime.ElapsedGameTime.Milliseconds / 500f;
                    }
                    if (realHealth < maxHealth)
                    {
                        realHealth += 2 * gameTime.ElapsedGameTime.Milliseconds / 500f;
                    }
                }
                currMana = (int)realMana;
                currHealth = (int)realHealth;
                if (realHealth <= 0)
                {
                    lives -= 1;
                    realHealth = 100;
                    realMana = 100;
                    gameState = GameState.Start;
                }
                if (realHealth <= 0)
                    realHealth=0;
                if (lives == 0)
                    gameState = GameState.Lose;
            #endregion
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            imageAngle += MathHelper.Pi * gameTime.ElapsedGameTime.Milliseconds / 500f;    // Increment the angle
            scaleAngle += MathHelper.Pi * gameTime.ElapsedGameTime.Milliseconds / 500f;    // Increment the scale
            // TODO: Add your update logic here
            //mercury.Update(gameTime);

            #region camera stuff
            MouseState mouseState = Mouse.GetState();

            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                float xdiff = mouseState.X - prevMouseState.X;
                
                camera.RotateY = -xdiff / 100;
                
            }
            
            
            
            #endregion
            #region Ammunition Updates
            foreach (RigidObject ball in ammunition)
                ball.Update(gameTime);
            #endregion
            #region Controls
            KeyboardState keyboardState = Keyboard.GetState();
                if (keyboardState.IsKeyDown(Keys.Escape))
                    this.Exit();
                #region Player Controls
                if (keyboardState.IsKeyDown(Keys.W) || keyboardState.IsKeyDown(Keys.Up))
                {
                    Vector3 newPosition = Cube.Position - Vector3.Transform(15 * Vector3.UnitZ, Cube.Rotation) * gameTime.ElapsedGameTime.Milliseconds / 500f;
                    if (bitmapWorld.GetElevationBilinear(new Vector2((50 + newPosition.X) / 100, (50 + newPosition.Z) / 100)) < 0.07f)
                    {
                        Cube.Update(gameTime, new KeyboardState(Keys.V));
                        Cube.Position = newPosition;
                        camera.Position = new Vector3(Cube.Position.X, Cube.Position.Y + 30, Cube.Position.Z + 1);
                        //3rd person
                        //camera.Position = camera.Position - Vector3.Transform(15 * Vector3.UnitZ, camera.Rotation) * gameTime.ElapsedGameTime.Milliseconds / 500f;
                    }
                    //else 
                    //{
                    //    Cube.Position += Vector3.Transform(60 * gameTime.ElapsedGameTime.Milliseconds / 500 * Vector3.UnitZ, Cube.Rotation);
                    //}
                }
                if (keyboardState.IsKeyDown(Keys.S) || keyboardState.IsKeyDown(Keys.Down))
                {
                    Vector3 newPosition = Cube.Position + Vector3.Transform(15 * Vector3.UnitZ, Cube.Rotation) * gameTime.ElapsedGameTime.Milliseconds / 500f;
                    if (bitmapWorld.GetElevationBilinear(new Vector2((50 + newPosition.X) / 100, (50 + newPosition.Z) / 100)) < 0.07f)
                    {
                        Cube.Update(gameTime, new KeyboardState(Keys.V));
                        Cube.Position = newPosition;
                        camera.Position = new Vector3(Cube.Position.X, Cube.Position.Y + 30, Cube.Position.Z + 1);
                        //3rd person
                        //camera.Position = camera.Position + Vector3.Transform(15 * Vector3.UnitZ, camera.Rotation) * gameTime.ElapsedGameTime.Milliseconds / 500f;
                    }
                    //else
                    //{
                    //    Cube.Position -= Vector3.Transform(60 * gameTime.ElapsedGameTime.Milliseconds / 500 * Vector3.UnitZ, Cube.Rotation); 
                    //}
                }
                if (keyboardState.IsKeyDown(Keys.A) || keyboardState.IsKeyDown(Keys.Left))
                {
                    Cube.Update(new GameTime(), keyboardState);
                    camera.RotateY = 0.05f;
                    //camera.Position = new Vector3(Cube.Position.X, Cube.Position.Y + 30, Cube.Position.Z + 1);
                    //3rd person
                    //camera.Position = camera.Position + Vector3.Transform(15 * Vector3.UnitX, camera.Rotation) * gameTime.ElapsedGameTime.Milliseconds / 500f;
                }
                if (keyboardState.IsKeyDown(Keys.D) || keyboardState.IsKeyDown(Keys.Right))
                {
                    Cube.Update(new GameTime(), keyboardState);
                    camera.RotateY = -0.05f;
                    //3rd person
                    //camera.Position = camera.Position - Vector3.Transform(15 * Vector3.UnitX, camera.Rotation) * gameTime.ElapsedGameTime.Milliseconds / 500f;
                }
            #endregion

                if (keyboardState.IsKeyDown(Keys.Add))
                    camera.FieldOfView -= 0.01f;
                if (keyboardState.IsKeyDown(Keys.Subtract))
                    camera.FieldOfView += 0.01f;
                if (keyboardState.IsKeyDown(Keys.Space))
                {
                    --currMana;
                    --currHealth;
                }
                #region Spell Controls
                    if (keyboardState.IsKeyDown(Keys.D1) && prevKeyboardState.IsKeyUp(Keys.D1))
                    {                       
                        currentSpell = spell.LookUp(spell1, currentSpell);
                    }
                    if (keyboardState.IsKeyDown(Keys.D2) && prevKeyboardState.IsKeyUp(Keys.D2))
                    {
                        currentSpell = spell.LookUp(spell2, currentSpell);
                    }
                    if (keyboardState.IsKeyDown(Keys.D3) && prevKeyboardState.IsKeyUp(Keys.D3))
                    {
                        currentSpell = spell.LookUp(spell3, currentSpell);
                    }
                    if (keyboardState.IsKeyDown(Keys.D4) && prevKeyboardState.IsKeyUp(Keys.D4))
                    {
                        currentSpell = spell.LookUp(spell4, currentSpell);
                    }
                #endregion
                #region Menu Controls
                if (keyboardState.IsKeyDown(Keys.P) && prevKeyboardState.IsKeyUp(Keys.P))
                        gameState = GameState.Pause;
                    if (keyboardState.IsKeyDown(Keys.O) && prevKeyboardState.IsKeyUp(Keys.O))
                        gameState = GameState.Play;
                   /* if (keyboardState.IsKeyDown(Keys.T) && prevKeyboardState.IsKeyUp(Keys.T))
                        gameState = GameState.Start;*/
                    /*if (keyboardState.IsKeyDown(Keys.) && prevKeyboardState.IsKeyUp(Keys.S))
                        gameState = GameState.spellInventory;*/
                #endregion
                if (keyboardState.IsKeyDown(Keys.Tab) && prevKeyboardState.IsKeyUp(Keys.Tab))
                {
                    if (currentSpell.manaCost != 0 && (realMana-currentSpell.manaCost) > 0)
                    {
                        LaunchBall();
                        realMana -= currentSpell.manaCost;
                    }
                }
            #endregion
                //camera.Position = new Vector3(Cube.Position.X, Cube.Position.Y + 3, Cube.Position.Z + 10);
                //camera.LookAt = Vector3.Normalize(Vector3.Subtract(Cube.Position, camera.Position));

                #region delete ammo
                //increment our delete counters
                for (int index = 0; index < deleteSphere.Count; index++)
                {
                    deleteSphere[index] += gameTime.ElapsedGameTime.Milliseconds;
                }


            //remove any expired spheres 
            if(deleteSphere.Count > 0)
                if(deleteSphere.First() > 3000) {
                    deleteSphere.RemoveAt(0);
                    if (ammunition.Count > 0)
                        ammunition.RemoveAt(0);
                }

            //make sure delete counters and balls are in sync
            while (deleteSphere.Count < ammunition.Count)
                deleteSphere.Add(0);
                #endregion



            #region collision
            List<Enemy> EnemiesToBeRem = new List<Enemy>();
            foreach (RigidObject bullet in ammunition)
            {
                // For each wall
                TestCollisionWall(bullet, WallTest.Position, WallTest.Scale);
                TestCollisionWall(bullet, Wall0.Position, Wall0.Scale);
                TestCollisionWall(bullet, Wall1.Position, Wall1.Scale);
                TestCollisionWall(bullet, Wall2.Position, Wall2.Scale);
                TestCollisionWall(bullet, Wall3.Position, Wall3.Scale);
                TestCollisionWall(bullet, Wall4.Position, Wall4.Scale);
                TestCollisionWall(bullet, Wall5.Position, Wall5.Scale);
                TestCollisionWall(bullet, Wall6.Position, Wall6.Scale);
                TestCollisionWall(bullet, Wall7.Position, Wall7.Scale);
                
                foreach (Enemy enemy in enemyList)
                {
                    if ((bullet.Position.X < (enemy.Position.X + 1)) && (bullet.Position.X > (enemy.Position.X - 1)) &&
                        (bullet.Position.Z < (enemy.Position.Z + 1)) && (bullet.Position.Z > (enemy.Position.Z - 1)))
                    {

                        EnemiesToBeRem.Add(enemy);
                    }
                }
                foreach (Enemy enemy in EnemiesToBeRem)
                {
                    enemyList.Remove(enemy);
                }
                EnemiesToBeRem.Clear();
            }
            foreach (RigidObject dead in deadAmmo)
            {
                if (deleteSphere.Count < 0)
                    deleteSphere.RemoveAt(0);
                ammunition.Remove(dead);
            }

            List<RigidObject> deadBalls = new List<RigidObject>();
            foreach (Enemy enemy in enemyList)
            {
                foreach (RigidObject bullet in enemy.balls)
                {
                    if ((bullet.Position.X < (Cube.Position.X + 0.75)) && (bullet.Position.X > (Cube.Position.X - 0.75)) &&
                        (bullet.Position.Z < (Cube.Position.Z + 0.75)) && (bullet.Position.Z > (Cube.Position.Z - 0.75)))
                    {
                        realHealth -= 10;
                        deadBalls.Add(bullet);
                    }
                }

                foreach (RigidObject ball in deadBalls)
                {
                    enemy.balls.Remove(ball);
                }

                //Vector3 newPosition = enemy.Position - Vector3.Transform(15 * Vector3.UnitZ, enemy.Rotation) * gameTime.ElapsedGameTime.Milliseconds / 500f;
                //if (bitmapWorld.GetElevationBilinear(new Vector2((50 + newPosition.X) / 100, (50 + newPosition.Z) / 100)) > 0.07f)
                //{
                    
                //    enemy.Position = newPosition;
                //}

            }

            
            #endregion

            prevMouseState = mouseState;
            prevKeyboardState = keyboardState;
            elements[gameState].Update(gameTime);
            if (enemyList.Count == 0)
                gameState = GameState.Win;
            foreach (Enemy enemy in enemyList)
            {
                enemy.Update(gameTime);
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            #region Useless to UI Development
            // Clear the screen
            GraphicsDevice.Clear(Color.Goldenrod);
            // Set the Depth Stencil State to default for 3D rendering
            GraphicsDevice.DepthStencilState = new DepthStencilState();
            // Setup some colors for lighting
            effect.EmissiveColor = new Vector3(1.0f, 1.0f, 1.0f);
            effect.DiffuseColor = new Vector3(1.0f, 1.0f, 1.0f);
            effect.SpecularColor = new Vector3(1.0f, 1.0f, 1.0f);
            effect.SpecularPower = 10;
            effect.LightingEnabled = false; // Enable lighting!
            // Some parameters for the Directional lighting
            effect.DirectionalLight0.Direction = new Vector3(0, -5, 1);
            effect.DirectionalLight0.SpecularColor = Vector3.One;
            // Provide the different matrices
            effect.View = camera.View;
            effect.Projection = camera.Projection;

            effect.World = plane.World;
            effect.Texture = plane.Texture;
            effect.TextureEnabled = true;
            plane.Draw(effect);

            //The cube
            effect.World = Cube.World;
            effect.Texture = Cube.Texture;
            effect.TextureEnabled = true;
            Cube.Draw(camera.View, camera.Projection);

/*            effect.World = wall.World;
            effect.Texture = wall.Texture;
            effect.TextureEnabled = true;
            wall.Draw(effect); */
            #endregion

            #region enemies
            foreach (Enemy enemy in enemyList)
            {
                effect.World = enemy.World;
                effect.Texture = enemy.Texture;
                effect.TextureEnabled = true;
                enemy.Draw(camera.View, camera.Projection);
            }
            #endregion


            #region Ball Updates
            foreach (RigidObject ball in ammunition)
            {
                effect.World = ball.World;
                effect.Texture = ball.Texture;
                effect.TextureEnabled = true;
                ball.Draw(effect);
            }
            #endregion
            #region Wall Updates
            foreach (RigidObject WallTest in Walls)
            {
                effect.World = WallTest.World;
                effect.Texture = WallTest.Texture;
                effect.TextureEnabled = true;
                WallTest.Draw(effect);
            }

            effect.World = Wall0.World;
            effect.Texture = Wall0.Texture;
            effect.TextureEnabled = true;
            Wall0.Draw(effect);

            effect.World = Wall1.World;
            effect.Texture = Wall1.Texture;
            effect.TextureEnabled = true;
            Wall1.Draw(effect);

            effect.World = Wall2.World;
            effect.Texture = Wall2.Texture;
            effect.TextureEnabled = true;
            Wall2.Draw(effect);
            
            effect.World = Wall3.World;
            effect.Texture = Wall3.Texture;
            effect.TextureEnabled = true;
            Wall3.Draw(effect);

            effect.World = Wall4.World;
            effect.Texture = Wall4.Texture;
            effect.TextureEnabled = true;
            Wall4.Draw(effect);


            effect.World = Wall5.World;
            effect.Texture = Wall5.Texture;
            effect.TextureEnabled = true;
            Wall5.Draw(effect);

            effect.World = Wall6.World;
            effect.Texture = Wall6.Texture;
            effect.TextureEnabled = true;
            Wall6.Draw(effect);

            effect.World = Wall7.World;
            effect.Texture = Wall7.Texture;
            effect.TextureEnabled = true;
            Wall7.Draw(effect);

            #endregion
            spriteBatch.Begin();    // First, start the sprite batch
            #region Health and Mana
                //Draws the lives
                for (int i = 0; i < lives; i++)
                {
                    Lives.targetRect = new Rectangle((heartPos.X+10) * i, (heartPos.Y+10) *i, heartPos.Width, heartPos.Height) ;
                    Lives.Draw(spriteBatch);
                }
                spriteBatch.Draw(Whitetext, FhealthRectangle, Color.Black);
                spriteBatch.Draw(Blacktext, FmanaRectangle, Color.Blue);
                spriteBatch.Draw(healthTexture, healthRectangle, Color.White);
                spriteBatch.Draw(healthFrame, new Rectangle(385+125,20,120*2+30, 43), Color.White);
                spriteBatch.Draw(Whitetext, manaRectangle, Color.Blue);
                spriteBatch.Draw(ManaFrame, new Rectangle(423 + 125, 55, maxMana+30, 27), Color.White);
                spriteBatch.DrawString(textFont, currHealth.ToString() + "/" + maxHealth.ToString(), new Vector2(healthRectangle.X+40, healthRectangle.Y-4), Color.White);
                spriteBatch.DrawString(textFont, currMana.ToString() + "/" + maxMana.ToString(), new Vector2(manaRectangle.X + 20, manaRectangle.Y - 4), Color.White);
                for (int i = 0; i < 5; i++)
                {
                    spriteBatch.DrawString(textFont, spellbuttons[i], new Vector2(380 + (80 * i), 333), Color.Red);
                    if (i < 4)
                        spriteBatch.Draw(iconList[i], new Rectangle(375 + (80 * i), 355, 75, 75), Color.White);
                    spriteBatch.Draw(spellSelect, new Rectangle(375 + (80 * i), 355, 80, 80), Color.White);
                }
            
                spriteBatch.Draw(currentSpell.Icon, new Rectangle(375 + (80 * 4), 355, 75, 75), Color.White);
                Healthbar.Draw(spriteBatch);
            #endregion
               
            elements[gameState].Draw(spriteBatch, Vector2.Zero);
            spriteBatch.End();

            base.Draw(gameTime);
        }

        //Allen 
        #region Collision
        public void TestCollisionWall(RigidObject body, Vector3 position, Vector3 scale)
        {
            
            for (int j = 0; j < indices.Length / 3; j++)
            {
                TestCollision(body, vertices[indices[j * 3]] * scale + position, vertices[indices[j * 3 + 1]] * scale + position, vertices[indices[j * 3 + 2]] * scale + position);

            }
        }

        public void TestCollision(RigidObject first, Vector3 A, Vector3 B, Vector3 C)
        {
            // First, make the sphere center our origin
            A = A - first.Position;
            B = B - first.Position;
            C = C - first.Position;
            // Find the surface normal for the triangle (using a cross product)
            Vector3 normal = Vector3.Cross(B - A, C - A);
            normal.Normalize();
            // Find how far away the sphere is from the triangle
            // We use a dot prouct to "project" Vector A on the normal
            float separation = (float)Math.Abs(Vector3.Dot(A, normal));
            // If the sphere is too far away, then quit
            if (separation > first.Scale.X)
                return;
            // Good, so now it is in range! Find the contact point on the plane of the triangle
            Vector3 P = separation * normal;
            // Esoteric: Determine if the point is actually in the triangle
            if (Vector3.Dot(Vector3.Cross(B - A, P - A), Vector3.Cross(C - A, P - A)) > 0 ||
                Vector3.Dot(Vector3.Cross(B - C, P - C), Vector3.Cross(A - C, P - C)) > 0)
                return;
            // If the sphere is actually moving away, then don't bother (dot product here
            if (Vector3.Dot(first.Velocity, normal) > 0)
                return;
            // Finally, add the impulse force to the sphere if it's bouncy
            if (spell.doesItBounce(first.Texture))
                first.AddForce = -2 * Vector3.Dot(first.Velocity, normal) * normal * first.Mass;
            else
                deadAmmo.Add(first);

        }
        #endregion
        private void LaunchBall()
        {
            RigidObject ball = new RigidObject();
            ball.Model = currentSpell.spellObject.Model;
            ball.Texture = currentSpell.texture;
            ball.Scale *= (float)(1f);
            ball.Velocity = Vector3.Transform(-50f * Vector3.UnitZ, Cube.Rotation);
            ball.Position = new Vector3(Cube.Position.X, Cube.Position.Y + 5, Cube.Position.Z);

            ammunition.Add(ball);
            deleteSphere.Add(0);
        }
        private void StartNewGame(object sender, EventArgs args)
        {
            gameState = GameState.Play;
            MediaPlayer.Play(background1);
        }
        private void EndGame(object sender, EventArgs args)
        {
            Exit();
        }
        private void Story(object sender, EventArgs args)
        {
            gameState = GameState.Story;
          
        }
        
        private void MainMenu(object sender, EventArgs args)
        {
            gameState = GameState.Menu;
        }
        
        private void Credits(object sender, EventArgs args)
        {
            gameState = GameState.Credits;
        }
        private void Information(object sender, EventArgs args)
        {
            gameState = GameState.Information;
        }
        /*private void spellList(object sender, EventArgs args)
        {
            gameState = GameState.spellInventory;
        }*/
    }
}
