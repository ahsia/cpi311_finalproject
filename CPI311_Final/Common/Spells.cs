﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
//using Microsoft.VisualBasic.PowerPacks;
using Common;


namespace Common
{
    public class Spells
    {
        KeyboardState prevkeyboardState;
        public Dictionary<int, Spell> spellBook;
        public List<Spell> spellList;

        #region Spells
            public Spell fire;
            public Spell air;
            public Spell water;
            public Spell earth;
            public Spell energy;
            public Spell lava;
            public Spell mud;
            public Spell sand;
            public Spell glass;
            public Spell obsidian;
            public Spell none;
        #endregion
        ContentManager Content;
        SpellObject ball;
       

        public Spells(ContentManager content)
        {
            spellBook = new Dictionary<int, Spell>();
            spellList = new List<Spell>();
            Content = content;
            
            LoadContent();
            Initialize();
        }

        public void Initialize()
        {
            spellList.Add(fire);
            spellList.Add(air);
            spellList.Add(earth);
            spellList.Add(water);
            spellList.Add(lava);
            spellList.Add(obsidian);
            spellList.Add(sand);
            spellList.Add(glass);
            spellList.Add(energy);
            spellList.Add(mud);
            spellList.Add(none);

            spellBook.Add(new SpellCombo(fire, air).GetHashCode(), energy);
            spellBook.Add(new SpellCombo(earth, fire).GetHashCode(), lava);
            spellBook.Add(new SpellCombo(earth, water).GetHashCode(), mud);
            spellBook.Add(new SpellCombo(air, earth).GetHashCode(), sand);
            spellBook.Add(new SpellCombo(sand, fire).GetHashCode(), glass);
            spellBook.Add(new SpellCombo(lava, water).GetHashCode(), obsidian);

            spellBook.Add(new SpellCombo(fire, none).GetHashCode(), fire);
            spellBook.Add(new SpellCombo(air, none).GetHashCode(), air);
            spellBook.Add(new SpellCombo(water, none).GetHashCode(), water);
            spellBook.Add(new SpellCombo(earth, none).GetHashCode(), earth);
            spellBook.Add(new SpellCombo(energy, none).GetHashCode(), energy);
            spellBook.Add(new SpellCombo(mud, none).GetHashCode(), mud);
            spellBook.Add(new SpellCombo(lava, none).GetHashCode(), lava);
            spellBook.Add(new SpellCombo(sand, none).GetHashCode(), sand);
            spellBook.Add(new SpellCombo(glass, none).GetHashCode(), glass);
            spellBook.Add(new SpellCombo(obsidian, none).GetHashCode(), obsidian);

            

        }
        public void Update(Microsoft.Xna.Framework.GameTime gameTime, KeyboardState keyboardState)
        {
            
            prevkeyboardState = keyboardState;
        }
        protected void LoadContent() 
        {
            ball = new SpellObject();
            ball.Model = Content.Load<Model>("Models/Sphere");
            ball.Texture = Content.Load<Texture2D>("Textures/More Textures(Allen)/Fireball");
            fire = new Spell("Fireball", "Offense", Content.Load<Texture2D>("Textures/Icons/Fireball"), ball, 10, 15, 3, 1, 0, 0, Content.Load<Texture2D>("Textures/More Textures(Allen)/Fireball"), false);
            ball.Texture = Content.Load<Texture2D>("Textures/Icons/Air bolt");
            air = new Spell("Air Blast", "Offense", Content.Load<Texture2D>("Textures/Icons/Air bolt"), ball, 10, 15, 3, 1, 0, 0, Content.Load<Texture2D>("Textures/Icons/Air bolt"), true);
            ball.Texture = Content.Load<Texture2D>("Textures/More Textures(Allen)/Water");
            water = new Spell("Waterblast", "Offense", Content.Load<Texture2D>("Textures/Icons/Waterball"), ball, 10, 15, 3, 1, 0, 0, Content.Load<Texture2D>("Textures/More Textures(Allen)/Water"), true);
            ball.Texture = Content.Load<Texture2D>("Textures/Icons/earth");
            earth = new Spell("Earth Shield", "Defense", Content.Load<Texture2D>("Textures/Icons/earth"), ball, 10, 15, 3, 1, 0, 0, Content.Load<Texture2D>("Textures/Icons/earth"), false);
            ball.Texture = Content.Load<Texture2D>("Textures/More Textures(Allen)/Energy");
            energy = new Spell("Energyblast", "Offense", Content.Load<Texture2D>("Textures/Icons/Energy"), ball, 10, 15, 3, 1, 0, 0, Content.Load<Texture2D>("Textures/More Textures(Allen)/Energy"), true);
            ball.Texture = Content.Load<Texture2D>("Textures/More Textures(Allen)/Lava");
            lava = new Spell("Lavaball", "Offense", Content.Load<Texture2D>("Textures/Icons/Lava"), ball, 10, 15, 3, 1, 0, 0, Content.Load<Texture2D>("Textures/More Textures(Allen)/Lava"), false);
            ball.Texture = Content.Load<Texture2D>("Textures/More Textures(Allen)/Mud");
            mud = new Spell("Mud Blast", "Offense", Content.Load<Texture2D>("Textures/Icons/mud"), ball, 10, 15, 3, 1, 0, 0, Content.Load<Texture2D>("Textures/More Textures(Allen)/Mud"), true);
            ball.Texture = Content.Load<Texture2D>("Textures/More Textures(Allen)/sand");
            sand = new Spell("Sand Blast", "Defense", Content.Load<Texture2D>("Textures/Icons/Sand"), ball, 10, 15, 3, 1, 0, 0, Content.Load<Texture2D>("Textures/More Textures(Allen)/sand"), false);
            ball.Texture = Content.Load<Texture2D>("Textures/More Textures(Allen)/Glass");
            glass = new Spell("Glass Blast", "Offense", Content.Load<Texture2D>("Textures/Icons/glass"), ball, 10, 15, 3, 1, 0, 0, Content.Load<Texture2D>("Textures/More Textures(Allen)/Glass"), false);
            ball.Texture = Content.Load<Texture2D>("Textures/More Textures(Allen)/obsidian256");
            obsidian = new Spell("Obsidian Shield", "Defense", Content.Load<Texture2D>("Textures/Icons/Obsidian1"), ball, 10, 15, 3, 1, 0, 0, Content.Load<Texture2D>("Textures/More Textures(Allen)/obsidian256"), true);
            none = new Spell("None", "None", Content.Load<Texture2D>("Textures/Icons/Lightning"), null, 0, 0, 0, 0, 0);
        }

        public Spell LookUp(SpellCombo combo)
        {
            if(spellBook.ContainsKey(combo.GetHashCode()))
                return spellBook[combo.GetHashCode()];
            else
                return none;
            
        }

        public bool doesItBounce(Texture2D texture)
        {
            foreach (Spell spell in spellList)
            {
                if (spell.texture == texture)
                    return spell.bouncy;
            }

            return false;
        }

        public Spell LookUp(Spell spell1, Spell spell2)
        {
            return LookUp(new SpellCombo(spell1, spell2));
        }


        public class SpellCombo
        {
            public Tuple<Spell, Spell> combo { get; set; }

            public SpellCombo(Spell spell1, Spell spell2)
            {
                combo = new Tuple<Spell, Spell>(spell1, spell2);
            }

            public override int GetHashCode()
            {
                if (combo != null)
                {
                    if (combo.Item1 != null && combo.Item2 != null)
                        if (combo.Item1.spellName.CompareTo(combo.Item2.spellName) > 0)
                            return (combo.Item1.spellName + combo.Item2.spellName).GetHashCode();
                        else
                            return (combo.Item2.spellName + combo.Item1.spellName).GetHashCode();
                }
                return base.GetHashCode();
                

            } 


            public override bool Equals(object obj)
            {
                if (!(obj is SpellCombo)) return false;

                SpellCombo other = (SpellCombo)obj;

                if (this.combo.Item1 == other.combo.Item1 && this.combo.Item2 == other.combo.Item2) return true;
                else if (this.combo.Item2 == other.combo.Item1 && this.combo.Item1 == other.combo.Item2) return true;
                else return false;

            }
        }
    }
}
