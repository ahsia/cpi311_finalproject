﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace Common
{
    public class Spell
    {
        public String spellName { get; set; }//Name of the Spell
        public String spellType { get; set; }//Spell Class
        public Texture2D Icon { get; set; }  //Spell Icon
        public int manaCost { get; set; }    //amount of mana it costs
        public int spellRadius { get; set; } //Spell range
        public int spellDamage { get; set; } //amount of damage a sell causes
        public int coolDown { get; set; }    //How long before you can use it again
        public int spellTier { get; set; }   //Spell Ranks
        public int Protection { get; set; }  //for defense spells
        public Texture2D texture { get; set; }
        public SpellObject spellObject {get; set;}
        public bool bouncy { get; set; }

        public Spell(String Name, String type, Texture2D icon, SpellObject Object, int cost = 0, int radius = 0, int damage = 0, int CD = 0, int tier = 0, int protection = 0, Texture2D texture = null, bool bouncy = false)
        {
            spellName = Name;
            spellType = type;
            Icon = icon;
            manaCost = cost;
            spellRadius = radius;
            spellDamage = damage;
            coolDown = CD;
            spellTier = tier;
            Protection = protection;
            spellObject = Object;
            this.texture = texture;
            this.bouncy = bouncy;
        }
        public void Update()
        {
            
        }

        public override int GetHashCode()
        {
            return (this.spellName + this.spellType).GetHashCode();
        }


        public override bool Equals(object obj)
        {
            if(!(obj is Spell)) return false;

            if(this.spellName == ((Spell)obj).spellName) return true;
            else return false;
        }
    }
}
