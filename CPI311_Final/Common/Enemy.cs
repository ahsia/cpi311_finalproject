﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Common;


namespace Common
{
    public class Enemy : AnimatedObject
    {
        public Spell attackSpell { get; set; }
        public SpellObject spellObject { get; set; }
        public Vector3 origPosition { get; set; }
        public int health { get; set; }
        public int playerRange { get; set; } //the Range from which the enemy notices the player
        public int spellRange { get; set; } // How much range the actual spell has
        public bool inRange { get; set; }
        public int mana { get; set; }
        public GameTime castTime { get; set; }
        public int castSpeed { get; set; }
        public Model spellModel { get; set; }
        public Texture2D spellTexture { get; set; }
        public Player player { get; set; }
        public List<RigidObject> balls { get; set; }

        private float lastCast = 1f;
        

        public Enemy(Spell spell)
        {
            playerRange = 20;//change from 30 to 10 (Allen)
            spellRange = 7;//change fro 10 to 5 (Allen)
            balls = new List<RigidObject>();
            spellModel = spell.spellObject.Model;
            spellTexture = spell.Icon;
            castSpeed = 1000;
            
        }

        public void TargetPlayer(GameTime gameTime)
        {

            lastCast += gameTime.ElapsedGameTime.Milliseconds;
            
            //If the player is in Range of enemy, 
            //Enemy will move towards player.
            float distance = Vector3.Distance(player.Position, this.Position);
            if (distance <= playerRange)
            {
                // the new forward vector, so the avatar faces the target
                Vector3 newForward = Vector3.Normalize(Position - player.Position);
                // calc the rotation so the avatar faces the target
                Rotation = GetRotation(Vector3.Forward, -newForward, Vector3.Up);


                //if the player is within range of the 
                // enemy's spell, it will attack.
                if (distance <= spellRange)
                    if(lastCast >= castSpeed)
                        {
                            AttackPlayer(gameTime);
                            lastCast = 0;
                        }
                
                                    
                this.Position -= Vector3.Transform(5 * gameTime.ElapsedGameTime.Milliseconds / 500f * Vector3.UnitZ, this.Rotation);
                
                
            }
            else
            { 
                
            }
        }

        public static Quaternion GetRotation(Vector3 source, Vector3 dest, Vector3 up)
            {
                float dot = Vector3.Dot(source, dest);

                if (Math.Abs(dot - (-1.0f)) < 0.000001f)
                {
                    // vector a and b point exactly in the opposite direction, 
                    // so it is a 180 degrees turn around the up-axis
                    return new Quaternion(up, MathHelper.ToRadians(180.0f));
                }
                if (Math.Abs(dot - (1.0f)) < 0.000001f)
                {
                    // vector a and b point exactly in the same direction
                    // so we return the identity quaternion
                    return Quaternion.Identity;
                }

                float rotAngle = (float)Math.Acos(dot);
                Vector3 rotAxis = Vector3.Cross(source, dest);
                rotAxis = Vector3.Normalize(rotAxis);
                return Quaternion.CreateFromAxisAngle(rotAxis, rotAngle);
            }

        public void AttackPlayer(GameTime gameTime)
        {
            RigidObject ball = new RigidObject();
            ball.Model = spellModel;
            ball.Texture = spellTexture;
            ball.Scale *= (float)(0.5f);
            ball.Velocity = Vector3.Transform(-50f * Vector3.UnitZ, this.Rotation);
            ball.Position = new Vector3(this.Position.X, this.Position.Y + 5, this.Position.Z);

            balls.Add(ball);
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            
            TargetPlayer(gameTime);

            foreach (RigidObject ball in balls)
            {
                ball.Update(gameTime);
            }
            
        }

        public override void Draw(Matrix view, Matrix projection)
        {
            base.Draw(view, projection);

           foreach(RigidObject ball in balls) {
                ball.Draw(view, projection);
           }

        }


    }
}
