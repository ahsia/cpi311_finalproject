﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Common;
namespace Common
{
    public class InfoBar : UIElement
    {
        public Texture2D Bar { get; set; }
        public Rectangle targetRect {get; set;}
        public Color Color {get; set;}

       public InfoBar(Texture2D bar, Rectangle position, Color color)
        {
            Bar = bar;
            targetRect = position;
            Color = color;
        }
        //mainly used to draw the UI Frames
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Bar, targetRect, Color);
        }
        //Draws the frame filler using the same position as the frame
        public void Draw(SpriteBatch spriteBatch, Texture2D fillColor)
        {
            spriteBatch.Draw(fillColor, targetRect, Color);
        }
        //Draws the filler using a custom position
        public void Draw(SpriteBatch spriteBatch, Texture2D fillColor, Rectangle fillPos)
        {
            spriteBatch.Draw(fillColor, fillPos, Color);
        }
    }
}
