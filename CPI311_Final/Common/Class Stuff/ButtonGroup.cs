﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Common;
namespace Common
{
    public class ButtonGroup : UIElement
    {
        public int Current { get; set; }
        public KeyboardState prevKeyboardState = Keyboard.GetState();
        public ButtonGroup(Texture2D background = null,
            Rectangle? bounds = null)
            : base(background, bounds)
        {
            
            Current = 0;
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            if (keyboardState.IsKeyDown(Keys.Down) && prevKeyboardState.IsKeyUp(Keys.Down))
                Current = (Current + 1) % Children.Count;
            if (keyboardState.IsKeyDown(Keys.Up) && prevKeyboardState.IsKeyUp(Keys.Up))
                Current = (Current - 1 + Children.Count) % Children.Count;
            if (keyboardState.IsKeyDown(Keys.Enter))
                Children[Current].Click();
            foreach (UIElement element in Children)
                element.Color = Color.Gray;
            Children[Current].Color = Color.White;
            prevKeyboardState = keyboardState;
            base.Update(gameTime);
        }
    }
}
