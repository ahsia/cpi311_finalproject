﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Common
{
    public class Player : AnimatedObject
    {
        Texture2D texture;
        public Rectangle rectangle;
        Vector2 position;
        float speed = 10f;
        public int health;

        public void Update(Microsoft.Xna.Framework.GameTime gameTime, KeyboardState keyboardState)
        {
            //rectangle = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
            
        
            base.Update(gameTime);

            
            if (keyboardState.IsKeyDown(Keys.W) || keyboardState.IsKeyDown(Keys.Up))
            {
                this.Position -= Vector3.Transform(speed * gameTime.ElapsedGameTime.Milliseconds/500 * Vector3.UnitZ, this.Rotation);

            }
            if (keyboardState.IsKeyDown(Keys.S) || keyboardState.IsKeyDown(Keys.Down))
            {
                this.Position += Vector3.Transform(speed * gameTime.ElapsedGameTime.Milliseconds/500 * Vector3.UnitZ, this.Rotation);

            }
            if (keyboardState.IsKeyDown(Keys.A) || keyboardState.IsKeyDown(Keys.Left))
            {
                this.RotateY = 0.05f;
           
            }
            if (keyboardState.IsKeyDown(Keys.D) || keyboardState.IsKeyDown(Keys.Right))
            {
                this.RotateY = -0.05f;
                
            }

        }
    }
}
